public class Bank {
	private Operation income;
	private Operation transfer;

	public Bank() {
	}

	public void income(Account account, double ammount) {
		income = new Income(account, ammount);
		account.doOperation(income);

	}

	public void transfer(Account from, Account to, double ammount) {
		transfer = new Transfer(from, to, ammount);
		from.doOperation(transfer);

	}
}
