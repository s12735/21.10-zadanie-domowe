
public class Account {
	private String number;
	private double ammount;
	private History history;

	public Account(String number) {
		this.number = number;
		this.ammount = 0;
		this.history = new History();
	}

	public void add(double ammount) {
		this.ammount += ammount;
		this.history.add(new HistoryLog("+" + ammount, OperationType.Income));
	}

	public void substarct(double ammount) {
		this.ammount -= ammount;
		this.history.add(new HistoryLog("-" + ammount, OperationType.Outcome));
	}

	public void doOperation(Operation operation) {
		operation.execute(this);
	}

	public String getNumber() {
		return number;
	}

	public double getAmmount() {
		return ammount;
	}
	
	public History getHistory(){
		return history;
	}
	
	public void showLog(){
		this.history.showLog();
	}

}
