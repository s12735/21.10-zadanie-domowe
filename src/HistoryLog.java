import java.text.SimpleDateFormat;
import java.util.Date;

public class HistoryLog {
	private Date date;
	private String title;
	private OperationType operationType;

	SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");

	public HistoryLog(String title, OperationType operationType) {
		this.date = new Date();
		this.title = title;
		this.operationType = operationType;
	}

	public String toString() {
		StringBuilder s = new StringBuilder();
		s.append("Date: ");
		s.append(df.format(date));
		s.append(", operation type: ");
		s.append(operationType);
		s.append(", ");
		s.append(this.title);

		return s.toString();
	}

}
