
public class Income extends Operation {

	private Account account;
	private double ammount;

	public Income(Account account, double ammount) {
		this.account = account;
		this.ammount = ammount;
	}

	@Override
	public void execute(Account account) {
		this.account.add(ammount);
	}

	
}
