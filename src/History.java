import java.util.ArrayList;
import java.util.List;

public class History {
	private List<HistoryLog> logs;

	public History() {
		this.logs = new ArrayList<>();
	}

	public void add(HistoryLog log) {
		this.logs.add(log);
	}

	public List<HistoryLog> getLogs() {
		return logs;
	}

	public void showLog(){
		
		for (HistoryLog logs : logs){
			System.out.println(logs.toString());
		}
	}
}
