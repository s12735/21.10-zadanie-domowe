
public class Transfer extends Operation {
	private Account source, destination;
	private double ammount;

	public Transfer(Account source, Account destination, double ammount) {
		this.source = source;
		this.destination = destination;
		this.ammount = ammount;
	}

	@Override
	public void execute(Account account) {
		source.substarct(ammount);
		destination.add(ammount);

	}

}
